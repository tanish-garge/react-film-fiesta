import React from 'react'
import { AppRoutes, Footer, Header } from './components'


export default function App() {
  return (
    <div className='dark:bg-gray-800 min-h-[99vh]'>
      < Header />
        <AppRoutes />
      <Footer />
    </div>
  )
}
