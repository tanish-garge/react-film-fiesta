import React, { useEffect } from 'react'
import useFetch from '../hooks/useFetch'
import { MovieCard, MovieCardSkeleton } from '../components';
import { useDynamicTitle } from '../hooks/useDynamicTitle';

export const MoviePage = ({ apiPath, title}) => {
  const BASE_API_PATH = process.env.REACT_APP_API_URL; // process is coming from Node js because normal js don't have access of processes(OS wale)
  const { data: movies, error, isLoading, setUrl } = useFetch();

  useDynamicTitle(title);

  useEffect (() => {
    setUrl(`${BASE_API_PATH}${apiPath}?api_key=${process.env.REACT_APP_API_KEY}`);
  }, [apiPath]);

  function renderSkeletons(count) {
    const skeletons = [];
    for(let i=1; i<= count; i++) {
      skeletons.push(<MovieCardSkeleton key={i} />);
    }
    return skeletons;
  }
  
  return (
    <main>
      <div className="flex flex-wrap justify-start">
        {
          isLoading && renderSkeletons(6)
        }
        {
          movies && movies.results.map(movie => <MovieCard movie={movie} key={movie.id} />)
        }
      </div>
    </main>
  )
}
